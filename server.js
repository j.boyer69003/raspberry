const express = require( 'express', )
const path = require('path',);
const bodyParser = require('body-parser',)
const gpiop = require('rpi-gpio',).promise;
const { exec } = require('child_process');

const rateLimit = require('express-rate-limit',);

const app = express()
const port = 3000
// const limiter = rateLimit({
//   windowMs: 15 * 60 * 1000, // 15 minutes
//   max: 500, // limit each IP to 100 requests per windowMs
// }, );
  
// app.enable('trust proxy',);
//  apply to all requests
// app.use(limiter,);
app.use( express.static( 'public', ), );
app.use(bodyParser.urlencoded({ extended: false, },),)
app.use(bodyParser.json(),)


let LED = false

app.post( '/wifi', async ( req, res, ) => {
  try {
    var ssid_command = "sed -i \'s/ssid=\"SFR_C4A0\"/ssid=\"" + req.body.ssid + "\"/\' /etc/wpa_supplicant/wpa_supplicant.conf";
    var psk_command = "sed -i \'s/psk=\"r5antrymaggiumbutrid\"/psk=\"" + req.body.password + "\"/\' /etc/wpa_supplicant/wpa_supplicant.conf";
    require('child_process').exec(ssid_command, function (msg) {
      console.log(msg) 
    });
    require('child_process').exec(psk_command, function (msg) {
      console.log(msg) 
    } );
    require('child_process').exec('sudo service network-manager restart', function (msg) {
      console.log(msg) 
    } );
    return  res.json(true);
  } catch ( e ) {
    console.log('Error: ', e.toString(),)
  }
} )
  
app.post( '/api', async ( req, res, ) => {
  try {
    await gpiop.setup( 8, gpiop.DIR_OUT )
    await gpiop.setup( 7, gpiop.DIR_OUT )
    LED = !LED
    gpiop.write( 7, LED );
    gpiop.write( 8, LED );
    return res.json(true);

  } catch ( e ) {
    console.log('Error: ', e.toString(),)
  }
} )

app.post( '/led', async ( req, res, ) => {
  try {
    LED = !LED
    await gpiop.setup( 8, gpiop.DIR_OUT )
    await gpiop.setup( 7, gpiop.DIR_OUT )
    await gpiop.setup( 10, gpiop.DIR_OUT )
    gpiop.write( 7, LED );
    gpiop.write( 8, false );
    gpiop.write( 10, !LED );
    return res.json(true);

  } catch ( e ) {
    console.log('Error: ', e.toString(),)
  }
},)




app.listen( port, async () => {
  console.log( `Example app listening at http://localhost:${ port }`, )
} )

process.on('SIGINT', function () {
  process.exit( 0);
});
  