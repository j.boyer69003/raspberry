const express = require( 'express', )
const path = require('path',);
const bodyParser = require('body-parser',)
const gpiop = require('rpi-gpio',).promise;
const NodeWebcam = require( 'node-webcam' );
const cors = require('cors')

const app = express()
const port = 3001
app.use(express.static(__dirname + '/public'));
app.set( 'view engine', 'html' );
app.use( cors() )

app.use(bodyParser.urlencoded({ extended: false, },),)
app.use(bodyParser.json(),)


app.get( '/cam', async ( req, res, ) => {
   
  const opts = {

    //Picture related
    width: 1280,
    height: 720,
    quality: 100,
    // Number of frames to capture
    // More the frames, longer it takes to capture
    // Use higher framerate for quality. Ex: 60
    frames: 60,
    //Delay in seconds to take shot
    //if the platform supports miliseconds
    //use a float (0.1)
    //Currently only on windows
    delay: 0,
    //Save shots in memory
    saveShots: false,
    // [jpeg, png] support varies
    // Webcam.OutputTypes
    output: 'jpeg',
    //Which camera to use
    //Use Webcam.list() for results
    //false for default device
    device: false,
    // [location, buffer, base64]
    // Webcam.CallbackReturnTypes
    callbackReturn: 'location',
    //Logging
    verbose: false
  };
    
    

  //Creates webcam instance
  // var Webcam = NodeWebcam.create( opts );
  //Will automatically append location output type

  opts.callbackReturn = 'base64'

  NodeWebcam.capture( 'test_picture', opts, function ( err, data ) {
    return res.send(data)
  });

  
},)


app.listen( port, async () => {
  console.log( `Example app listening at http://localhost:${ port }`, )
} )
