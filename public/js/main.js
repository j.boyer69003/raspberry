const url = '192.168.1.34'


const vue = new Vue( {
    el: '#app',
    vuetify: new Vuetify( {
        theme: { dark: true },
    } ),
    data: {
        message: 'Hello Vue !',
        switchLight: false,
        wifi: false,
        valid: false,
        snackbar: false,
        notify: '',
        ssid: '',
        password: '',
        cam: false,
        img: ""
    },
    methods: {
        loadImg ()
        {
            this.cam = true
           
            setInterval( () =>
            {
                axios.get( 'http://raspberrypi.local:3001/cam').then( ( res ) =>
                {
                    this.img = res.data
                } ) 
            }, 2000)
        },
        validate ()
        {
                axios.post( '/wifi', {
                    ssid: this.ssid,
                    password: this.password
                } ).then( ( res ) =>
                {
                    this.notify = 'Wifi is updated!'
                    this.snackbar = true
                } );
         
        }
    },
    watch: {
        switchLight ( val )
        {
            axios.post( '/api', {
                switchLight: val,
            } )
            
        }
    }
} );


