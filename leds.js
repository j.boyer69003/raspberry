const Gpio = require('pigpio').Gpio;


const main = async () => {
  const led = new Gpio(21, {mode: Gpio.OUTPUT});
  led.digitalWrite( 1 );

  var ws281x = require('rpi-ws281x');
 
  // One time initialization
  ws281x.configure({leds:256, gpio: 18, type: 'rgb'});
 
  // Create my pixels
  var pixels = new Uint32Array( 256 );
    
  // Create a fill color with red/green/blue.
  var red = 255, green = 255, blue = 255;
  var color = ('2F' << 16) | ('95' << 16) | ('B2' << 16);

  for (var i = 0; i < 256; i++) {
    pixels[i] = 0xFF0000;
  }
    
 
  // Render pixels to the Neopixel strip
  ws281x.render(pixels);
}
main()